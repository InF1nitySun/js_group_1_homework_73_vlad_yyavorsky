const express = require('express');
const app = express();
const port = 8000;

const caesarSalad = require('caesar-salad');
const Caesar = caesarSalad.Caesar;
const ROT13 = caesarSalad.ROT13;
const ROT5 = caesarSalad.ROT5;
const ROT18 = caesarSalad.ROT18;
const ROT47 = caesarSalad.ROT47;
const Vigenere = caesarSalad.Vigenere;



const secret = 'test';

app.get('/', (req, res) => {
    console.log(`Hello !`);
    res.send(`Hello`);
});

app.get('/hello/', (req, res) => {
    console.log(`Hello, Guest!`);
    res.send(`Hello, Guest!`);
});

app.get('/hello/:name', (req, res) => {
    console.log(`Hello, ${req.params.name}!`);
    res.send(`Hello, ${req.params.name}!`);
});

app.get('/encode/:pass', (req, res) => {

    let CaesarPass = Caesar.Cipher(secret).crypt(req.params.pass);
    let VigenerePass = Vigenere.Cipher(secret).crypt(req.params.pass);

    let ROT13Pass = ROT13.Cipher().crypt(req.params.pass);
    let ROT5Pass = ROT5.Cipher().crypt(req.params.pass);
    let ROT18PAss = ROT18.Cipher().crypt(req.params.pass);
    let ROT47Pass = ROT47.Cipher().crypt(req.params.pass);

    res.send(`CaesarPass, \"${CaesarPass}\"\rROT13Pass: \"${ROT13Pass}\"\rROT5Pass: \"${ROT5Pass}\"\rROT18PAss: \"${ROT18PAss}\"\rROT47Pass: \"${ROT47Pass}\"\rVigenerePass: \"${VigenerePass}\"!`);
    // res.send(`твой пароль может быть таким, \"${CaesarPass}\"\rили таким: \"${ROT13Pass}\"\rили таким: \"${ROT5Pass}\"\rа может и таким: \"${ROT18PAss}\"\rвот еще забыл: \"${ROT47Pass}\"\rи последний: \"${VigenerePass}\"!`);

});

app.get('/decode/:pass', (req, res) => {

    let CaesarPass = Caesar.Decipher(secret).crypt(req.params.pass);
    let VigenerePass = Vigenere.Decipher(secret).crypt(req.params.pass);

    let ROT13Pass = ROT13.Decipher().crypt(req.params.pass);
    let ROT5Pass = ROT5.Decipher().crypt(req.params.pass);
    let ROT18PAss = ROT18.Decipher().crypt(req.params.pass);
    let ROT47Pass = ROT47.Decipher().crypt(req.params.pass);


    res.send(`CaesarPass, \"${CaesarPass}\"\rROT13Pass: \"${ROT13Pass}\"\rROT5Pass: \"${ROT5Pass}\"\rROT18PAss: \"${ROT18PAss}\"\rROT47Pass: \"${ROT47Pass}\"\rVigenerePass: \"${VigenerePass}\"!`);
    // res.send(`твой пароль может быть таким, \"${CaesarPass}\"\rили таким: \"${ROT13Pass}\"\rили таким: \"${ROT5Pass}\"\rа может и таким: \"${ROT18PAss}\"\rвот еще забыл: \"${ROT47Pass}\"\rи последний: \"${VigenerePass}\"!`);

});


app.listen(port, () => {
    console.log('server onLine ' + port);
});
